# Introduction to Data Structures
# [SECTION] Objects in Python
# Everything in Python is an object
# proof:

# isinstance() checks if the first argument is an instance or subclass of the second argument. Will output a boolean value

# int
print(f'int:- {isinstance(int, object)}')

#str
print(f'int:- {isinstance(str, object)}')

# bool
print(f'int:- {isinstance(False, object)}')
print(f'int:- {isinstance(True, object)}')

# function
def sample():
	pass
print(f'int:- {isinstance(sample, object)}')

# [Section] Parts of an object

x = 613

# 1. Reference Count
		# It deals with memory in the CPU. Represents the number of Python variables referring to a memory location
		# id() returns the memory address of the object
print(id(x))

# 2. Type
		# Refers to the kind of object like int, float, string, etc.
		# type() determines the type of an object
print(type(x))

# 3. Value
		# Actual value of an object stored in the memory
print(x)

# [Section] Immutable Objects
		# Can't be changed one it's completed. Most commonly used data types in Python are immutable (int, float, double, str, bool, complex, tuple)

# int data type is an immutable object
numA = 7
print(id(numA))
numA += 1
print(id(numA))

# When numA was assigned to numB, Python didn't create a new object, it just referenced the value of numA to numB. It saves memory.
numB = numA
print(numB)
# result: 8
print(id(numB))
# The object that was first created is now referenced in numB

# "is" checks whether two objects have the same memory address
isTrue = numB is numA
print(isTrue)

# [Section] Mutable Objects
# Objects that can be changed after creation (list, set, dictionary)

# list
nums = [1, 2, 3, 4, 5]
print("===Before Modifying===")
print(nums)
print(id(nums))
print('')

nums[0] += 1
print("===After Modifying===")
print(nums)
print(id(nums))
print('')

nums.append(6)
print("===After adding an element===")
print(nums)
print(id(nums))
print('')

# [Section] List
# Create a list
hero = ["Sniper", 50, True]
tasks = ["use_skills", "help_a_civilian", "play_with_friends", "cook_meat"]
stats = [25, 60, 63]
# Maintains a left-right prositional ordering
print(hero, tasks, stats)

# [Section] Indexing through the list
print(hero[0])
print(tasks[3])
print(tasks[-1])

# Sublist
skills = [hero, ["shrapanel", "headshot", "take_aim"]]
print(skills)
print(skills[0][1])

# [Section] Common List Literals and Operations

# Slice
# slice() function returns a slice object that is used to slice any sequence
# list[starting_index : stopping_index]
print(tasks[1:3])
print(tasks[:3]) # starts at index 0
print(tasks[1:]) # ends at the last index

print(len(tasks))
print(len(hero))

new_list = hero + tasks
print("Concatenated list")
print(new_list)

# extend() adds the specified list
# list.extend(list)
stats.extend(skills)
print(stats)

# Arrange one of the lists alphanumerically (sort())
		# Stretch reverse()
# Delete items inside one of the lists(del, pop())

# Sorting
# Ordering of items

# sort()
print("Sort items")
nums.sort()
print(nums)

# Reverse
nums.reverse()
print(nums)

# Shrinking
# Deleting items

# del keyword
del hero[1:3]
print("Delete items")
print(hero)

tasks.pop()
print(tasks)

# remove()
# Remove specified item in the list
stats.remove(63)
print(stats)

# Repeat
# * is used to repeat a list for a designated number of times
# list * repeated_times
hero = hero * 3
print(hero)

# iteration
print("iteration:")
for item in hero:
	print(item)

# Growing
# insert()
# Adds the value in a specific index/position
# insert(index, element)
stats.insert(0, 100)
print(stats)

# Searching
# Finding an element in a list

# index()
# returns an index; returns the index of the first element it finds
print(hero.index("Sniper"))

#count()
# returns the number of times an element appeared on the list
print(hero.count("Sniper"))

# [Section] Tuples
# Tuples work like lists in terms of grouping objects; The main difference is that tuples are immutable object
# Instead square brackets, tuples are defined by parenthesis

grades_tuples = (85, 87, 88)
print(grades_tuples)
print(type(grades_tuples))

# 2 is the only number that is prime and even
# if our tuple has only one value inside, we have to use a comma after the first value
prime_even = (2,)
print(type(prime_even))
# tuple() can also be used; can only take a single parameter
print(tuple("Python"))

# Nested tuples
album_songs = ("Red", ("State of Grace", "Red", "All Too Well"))
print(album_songs)

# indexing
print(album_songs[0])
print(album_songs[1][2])

# other operations for Tuple
continents = ("Asia", "Africa", "North America", "South America", "Antartica")
# slice
print(continents[2:4])
# len
print(len(continents))
# concatenation
continents = continents + ("Europe", "Oceania")
print(continents)
# iteration
for x in continents:
	print(x)
# repeat
continents = continents * 2
print(continents)
# membership
print("Asia" in continents)

# index()
print(continents.index("South America"))
# count
print(continents.count("Europe"))
continents = continents + ("Europe",)
print(continents.count("Europe"))
# tuples does not support Growing and Shrinking

# [Section] Dictionaries
# Used to store values in key:value pairs
player_one = {
	"username": "unknownhunter",
	"role": "assassin",
	"level": 11,
	"team": "in_team"
}
print(player_one)
# access the value of the key
print(player_one["username"])
# changing the value
player_one["level"] += 5
print(player_one)

# another way of creating dictionary
player_two = {}
player_two["username"] = "theTJC"
player_two["role"] = ["marksman", "tank"]
player_two["level"] = 30
player_two["team"] = {"name": "k_team", "members": 5}
print(player_two)

# accessing all keys
print(player_two.keys())

# accessing all values
print(player_two.values())

# using get()
print(player_two.get("role"))

# accessing from nested dictionary
print(player_two.get(("role"))[1])
print(player_two["role"][1])

# accessing from nested dictionary
print(player_two["team"]["name"])
print(player_two)

# update values
player_two.update({"team": {"name": "b_team"}})
print(player_two)

# deleting
del player_two["level"]
print(player_two)

# player_two.clear()
# print(player_two) # delete all keys and values, but the dictionary still exists

# existence
if ("role") in player_one.keys():
	print("Key exists")
	print(player_one.keys())
else:
	print("Key doesn't exist")